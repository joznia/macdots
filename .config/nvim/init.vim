" plugin stuff
call plug#begin('~/.vim/plugged')

	Plug 'itchyny/lightline.vim'
	Plug 'chrisbra/Colorizer'
	Plug 'frazrepo/vim-rainbow'
    Plug 'dracula/vim'
    Plug 'scrooloose/nerdtree'                         
    Plug 'tiagofumo/vim-nerdtree-syntax-highlight'     
    Plug 'ryanoasis/vim-devicons'                      
    Plug 'junegunn/vim-emoji'

call plug#end()

" general setup
set path+=**	
set nocompatible
set wildmenu	
set incsearch   
set hidden      
set nobackup    
set noswapfile  
set number relativenumber     
set expandtab                   
set smarttab                    
set shiftwidth=4                
set tabstop=4                   
set laststatus=2
set ignorecase
set smartcase
set mouse=nicr
set termguicolors
colorscheme dracula

let g:python_highlight_all = 1

au! BufRead,BufWrite,BufWritePost,BufNewFile *.org 
au BufEnter *.org            call org#SetOrgFileType()

set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar
:let g:colorizer_auto_color = 1
:let g:colorizer_auto_filetype='*'

set splitbelow splitright
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K
set fillchars+=vert:\ 

map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeDirArrowExpandable = '►'
let g:NERDTreeDirArrowCollapsible = '▼'
let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=1
let NERDTreeMinimalUI = 1
let g:NERDTreeWinSize=38

" colors
syntax enable

let g:lightline = {
      \ 'colorscheme': 'darcula',
      \ }

"   highlight LineNr           ctermfg=5    ctermbg=none    cterm=none
"   highlight CursorLineNr     ctermfg=7    ctermbg=8       cterm=none
"   highlight VertSplit        ctermfg=0    ctermbg=8       cterm=none
"   highlight Statement        ctermfg=2    ctermbg=none    cterm=none
"   highlight Directory        ctermfg=4    ctermbg=none    cterm=none
"   highlight StatusLine       ctermfg=7    ctermbg=8       cterm=none
"   highlight StatusLineNC     ctermfg=7    ctermbg=8       cterm=none
"   highlight NERDTreeClosable ctermfg=2
"   highlight NERDTreeOpenable ctermfg=8
"   highlight Comment          ctermfg=4    ctermbg=none    cterm=italic
"   highlight Constant         ctermfg=12   ctermbg=none    cterm=none
"   highlight Special          ctermfg=4    ctermbg=none    cterm=none
"   highlight Identifier       ctermfg=6    ctermbg=none    cterm=none
"   highlight PreProc          ctermfg=5    ctermbg=none    cterm=none
"   highlight String           ctermfg=12   ctermbg=none    cterm=none
"   highlight Number           ctermfg=1    ctermbg=none    cterm=none
"   highlight Function         ctermfg=1    ctermbg=none    cterm=none


