# ~/.zshrc

### PATH ###
export PATH=${HOME}/.local/bin:/opt/local/libexec/gnubin:/opt/local/bin:/opt/local/sbin:$PATH
export MANPATH=/opt/local/share/man:$MANPATH

### EXPORT ###
export EDITOR=nvim
export TERM=xterm-256color

### ALIASES ###
alias ..='cd ..' 
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'
alias df='df -h'                          # human-readable sizes
alias neofetch="neofetch | sed 's/port/MacPorts/g'"
alias vim=nvim
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias co='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias gs='git status'
alias tag='git tag'
alias newtag='git tag -a'
alias config="/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME"
alias caddup='config add -u'
alias cadd="config add"
alias cbranch='config branch'
alias ccheckout='config checkout'
alias ccommit='config commit -m'
alias cfetch='config fetch'
alias cpull='config pull origin'
alias cpush='config push origin'
alias cgs='config status'
alias ctag='config tag'
alias cnewtag='config tag -a'
alias merge='xrdb -merge ~/.Xresources'
alias jctl="journalctl -p 3 -xb"
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "
alias ytv-best="youtube-dl -f bestvideo+bestaudio "

### VIM KEYBINDS ###
bindkey -v

### PROMPT ###
PS1="%n@%F{blue}%m %F{magenta}%1~ %F{yellow}%#%F{white} "

# Simple fetch utility
pfetch
